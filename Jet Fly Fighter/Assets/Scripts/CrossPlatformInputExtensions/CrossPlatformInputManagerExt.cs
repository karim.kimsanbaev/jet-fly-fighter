using UnityStandardAssets.CrossPlatformInput;
using static UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager;

public static class CrossPlatformInputManagerExt
{
    public static VirtualAxis GetOrRegisterVirtualAxis(string axisName)
    {
        var axisExists = CrossPlatformInputManager.AxisExists(axisName);
        if (axisExists)
        {
            return CrossPlatformInputManager.VirtualAxisReference(axisName);
        }

        var axis = new CrossPlatformInputManager.VirtualAxis(axisName);
        CrossPlatformInputManager.RegisterVirtualAxis(axis);

        return axis;
    }
}