﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObserverControlText : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        var isTilt = PlayerPrefs.GetInt("IsTilt");
        GetComponent<Text>().text = isTilt == 1
        ? "Control Mode: Tilt"
        : "Control Mode: Accelerometer";
    }
}
