﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveWhenEnabled : MonoBehaviour
{
    public GameObject Observerable;
    public GameObject Target;
    public bool SetActiveState;

    // Update is called once per frame
    void Update()
    {
        if(Observerable == null)
        {
            return;
        }

        if(Observerable.activeInHierarchy)
        {
            Target.SetActive(SetActiveState);
        }
        else
        {
            Target.SetActive(!SetActiveState);
        }
    }
}
